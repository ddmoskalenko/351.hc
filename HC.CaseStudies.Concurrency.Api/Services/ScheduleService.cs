using HC.CaseStudies.Concurrency.Domain.Schedule.Models;
using HC.CaseStudies.Concurrency.Domain.Schedule.Repositories;
using HC.CaseStudies.Concurrency.Dto.Input;
using HC.CaseStudies.Concurrency.Dto.Response;
using HC.CaseStudies.Concurrency.Extensions;
using HC.CaseStudies.Concurrency.Infrastructure.DbContexts.Schedule;
using HC.CaseStudies.Concurrency.Services.Interfaces;

namespace HC.CaseStudies.Concurrency.Services;

public class SchedulesService: ServiceBase<IScheduleDbContext>, IScheduleService
{
    private readonly IScheduleRepository _scheduleRepository;
    private readonly IScheduleSync _scheduleSync;
    private static SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

    public SchedulesService(IScheduleDbContext dbContext, IScheduleRepository scheduleRepository, IScheduleSync scheduleSync) : base(dbContext)
    {
        _scheduleRepository = scheduleRepository;
        _scheduleSync = scheduleSync;
    }
    
    public async Task<ScheduleResponseDto> GetLatestScheduleForPlant(int plantCode)
    { 
        var currentDraftSchedule = await _scheduleRepository.GetLastUpdatedScheduleForPlant(plantCode);
        if (currentDraftSchedule == null)
        {
            throw new Exception($"There is no draft schedule for plant {plantCode}");
        }

        return currentDraftSchedule.MapToScheduleDto();
    }

    public async Task<ScheduleResponseDto> AddItemToSchedule(int scheduleId, ScheduleInputItemDto scheduleItem)
    {
        await _semaphore.WaitAsync();
        try
        {
            var scheduleWithId = await _scheduleRepository.GetScheduleById(scheduleId);
  
            scheduleWithId.AddItem(
                start: scheduleItem.Start,
                end: scheduleItem.End,
                cementType: scheduleItem.CementType,
                now: DateTime.UtcNow);
            await _scheduleRepository.Update(scheduleWithId);
            return scheduleWithId.MapToScheduleDto();
        }
        finally
        {
            _semaphore.Release();
        }
    }

    public async Task<ScheduleResponseDto> AddNewSchedule(int plantCode, List<ScheduleInputItemDto> scheduleInputItems)
    {
        var now = DateTime.UtcNow;
        var schedule = new Schedule(plantCode, now);
        if (scheduleInputItems != null)
        {
            foreach (var scheduleInputScheduleItem in scheduleInputItems)
            {
                schedule.AddItem(
                    start: scheduleInputScheduleItem.Start,
                    end: scheduleInputScheduleItem.End,
                    cementType: scheduleInputScheduleItem.CementType,
                    now: now);
            } 
        }

        await _scheduleRepository.Insert(schedule);
        return schedule.MapToScheduleDto();
    }

    public async Task<ScheduleResponseDto> ChangeScheduleItem(int scheduleId, int itemId, ScheduleInputItemDto scheduleInputItem)
    {
        var now = DateTime.UtcNow;
        var schedule = await _scheduleRepository.GetScheduleById(scheduleId);
        _scheduleSync.LockItemForUpdate(scheduleId, itemId);
        try
        {
            schedule.UpdateItem(itemId, scheduleInputItem.Start, scheduleInputItem.End, scheduleInputItem.CementType, now);
            await _scheduleRepository.Update(schedule);
        }
        catch {
            _scheduleSync.ReleaseItemForUpdate(scheduleId, itemId);
            throw;
        }
        return schedule.MapToScheduleDto();
    }
}