using HC.CaseStudies.Concurrency.Domain.Schedule.Repositories;
using HC.CaseStudies.Concurrency.Extensions;
using HC.CaseStudies.Concurrency.Infrastructure.DbContexts.Schedule;
using HC.CaseStudies.Concurrency.Infrastructure.Repositories.Schedule;
using HC.CaseStudies.Concurrency.Services;
using HC.CaseStudies.Concurrency.Services.Interfaces;

namespace HC.CaseStudies.Concurrency;

public class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers(x => x.AllowEmptyInputInBodyModelBinding = true);
        services.AddSingleton(_configuration);
        services.AddDatabase(_configuration);
        services.AddCors();
        services.AddScoped<IScheduleService, SchedulesService>();
        services.AddTransient<IScheduleRepository, ScheduleRepository>();
        services.AddSingleton<IScheduleSync, ScheduleSyncDictionary>();
    }
public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IScheduleDbContext dbContext)
    {
        app.UseRouting();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseExceptionHandler(errorApp =>
        {
            errorApp.Run(async context =>
            {
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = 500;
                    await context.Response.WriteAsync("");
            });
        });



    }

}