Preliminary
Basing "on The Problem and end points in readme (GET schedule allows getting the latest created schedule for a plant,POST schedule allows adding a schedule for a plant, POST schedule/items allows adding )
I had assumed that we don't have the functionality of changing existing or removing items in the schedule.
The fast solution was created which keeps fingerprint of the day for each Schedule (https://bitbucket.org/ddmoskalenko/351.hc/branch/with-fingerprints). 
But when I started to look at the endpoint in API and test, I found PUT and it made my assumption completly wrong.
I started just fixing the code for multithread cases.

For WhenTwoClientsAreChangingTheSingleItem issue I suggest using IScheduleSync singleton service. 
Currently, ConcurrentDictionary is used underhood but if we age going to scale our solution horizontally, make sense to implement IScheduleSync with lock-on Postgres level via native postgres SQL "LOCK FOR UPDATE" on specific item/row.
Again drawback is that it means we will strictly be coupled with Postgres DB, so the next improvement can be to sync locking between instances via common Redis cache.  
Say frankly, if correctly invalidate interval and use lock between adding and validating (which will guarantee that it will be processed sequentially) I don't think that processing two requests for update one item per once 
should be suspicious. 
But accoring the test one update should be compromised. 
var failures = itemAddResponses.ToList().Where(it => it.IsSuccessStatusCode == false);
failures.Count().Should().Be(1);
And IHMO, correctly to provide the process with both requests one after another with critical section only between addItemToRepo-ValidatingIntersection. 
BTW, the title of the test funtion GivenScheduleWithItem_WhenTwoClientsAreChangingTheSingleItem_ThenItemModificationShouldHappenInSequence name of the function is correct but assert failures.Count().Should().Be(1) dont follow the name(ThenItemModificationShouldHappenInSequence).   
I modified the code in order to pass the test but I think when ShouldHappenInSequence is more correctly. 
 
WhenTwoSimultaneousIdenticalAddItemRequests_ThenOneItemIsAddedAndTheOtherRejected
Having a critical section on the whole block of adding currently fix the issue. But I have to confess that it isn't an efficient solution.
Better to have a critical section for each schedule separately and for having multi instances solution in the Kubernetes cluster better to use my first  "day fingerprint logic"
Where locks looks like:
day 19.01.2022 000000000000000000000010101010101000001001001001000000000000000000000000001010101010100000100100100
day 20.01.2022 100000000000000000000000001010101000001001001001000000000000000000000000001010101010100000100100000

 And this fingerprint for days can be put into Redis or Postgres.  
 My initial idea works very well when we only add items because I provide always O(1) complexity check item intersection for the day(we have pre-calculated plan by minutes) and for 15 min intervals it cost us 96bit per day.
 Size of min interval (15 min) is not hardcoded and can be set via parameter to constructor. 
 