﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using HC.CaseStudies.Concurrency.Domain.Schedule.Repositories;
using HC.CaseStudies.Concurrency.Domain.Schedule.Exceptions;

namespace HC.CaseStudies.Concurrency.Infrastructure.Repositories.Schedule
{
    public class ScheduleSyncDictionary : IScheduleSync
    {
        private static ConcurrentDictionary<int, ConcurrentDictionary<int, bool>> ScheduleItemsInUpdate = new ConcurrentDictionary<int, ConcurrentDictionary<int, bool>>();

        public void LockItemForUpdate(int scheduleId, int itemId)
        {
            ScheduleItemsInUpdate.TryAdd(scheduleId, new ConcurrentDictionary<int, bool>());
            var schedule = ScheduleItemsInUpdate[scheduleId];
            if (!schedule.TryAdd(itemId, true))
               throw new InvalidPlanException("Changes for this ["+ itemId + "] in progress");
        }

        public void ReleaseItemForUpdate(int scheduleId, int itemId)
        {
            var schedule = ScheduleItemsInUpdate[scheduleId];
            bool value = false; 
            schedule.TryRemove(itemId, out value);
        }
    }
}
