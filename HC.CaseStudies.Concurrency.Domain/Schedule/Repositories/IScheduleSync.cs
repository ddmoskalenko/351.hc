﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HC.CaseStudies.Concurrency.Domain.Schedule.Repositories
{
    public interface IScheduleSync
    {
        void LockItemForUpdate(int scheduleId, int itemId);
        void ReleaseItemForUpdate(int scheduleId, int itemId);
    }
}
