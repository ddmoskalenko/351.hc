using HC.CaseStudies.Concurrency.Domain.Common.Repository;

namespace HC.CaseStudies.Concurrency.Domain.Schedule.Repositories;

public interface IScheduleRepository: IGenericRepository<Models.Schedule>
{

    public Task<Models.Schedule> GetLastUpdatedScheduleForPlant(int plantCode); 
    public Task<Models.Schedule> GetScheduleById(int scheduleId);
    
}