using System.Collections;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using HC.CaseStudies.Concurrency.Domain.Schedule.Models;

namespace HC.CaseStudies.Concurrency.Domain.Schedule.Validation;

public static class ScheduleItemValidation
{
    private static ConcurrentDictionary<int, ConcurrentDictionary<int, BitArray>> ScheduleItems = new ConcurrentDictionary<int, ConcurrentDictionary<int, BitArray>>();
    public static void ValidateDoesNotOverlapWithItems(this ScheduleItem currentItem, ICollection<ScheduleItem> ScheduleItems)
    {
        if (ScheduleItems.Any(scheduleItem => scheduleItem.ScheduleItemId != currentItem.ScheduleItemId && currentItem.Start < scheduleItem.End && scheduleItem.Start < currentItem.End))
        {
            throw new ValidationException("There is a conflict with the other planned item.");
        }
    }
}