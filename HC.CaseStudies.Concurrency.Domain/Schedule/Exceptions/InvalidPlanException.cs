﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HC.CaseStudies.Concurrency.Domain.Schedule.Exceptions
{
    public class InvalidPlanException : Exception
    {
        public InvalidPlanException(string message) : base(message)
        {
        }
        /// <summary>Message to user</summary>
        public string UserMessage => Message;
    }
}
